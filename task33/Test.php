<?php
namespace task33;

/**
Задача 33.2: Сделайте класс Test, использующий все три созданных нами трейта.
Сделайте в этом классе публичный метод getSum, возвращающий сумму результатов методов подключенных трейтов.*/

class Test
{
    use Trait1, Trait2, Trait3;

    public function getSum()
    {
        return $this->method1() + $this->method2() + $this->method3();
    }
}


