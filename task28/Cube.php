<?php

namespace task28;

/** Задача 28.3: Сделайте класс Cube (куб), который будет реализовывать интерфейс Figure3d.*/
class Cube implements Figure3d
{
    private const NUM = 6;

    private int $side;

    public function __construct($side)
    {
        $this->side = $side;
    }

    /**
     * @return int
     */
    public function getSide(): int
    {
        return $this->side;
    }

    public function getSurfaceSquare()
    {
        return self::NUM * ($this->getSide() * $this->getSide());
    }

    public function getVolume()
    {
        return $this->getSide() * $this->getSide() * $this->getSide();
    }
}