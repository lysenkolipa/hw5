<?php

namespace task28;

/**
 * Задача 28.2: Сделайте интерфейс Figure3d (трехмерная фигура),который будет иметь метод getVolume (получить объем) и
 * метод getSurfaceSquare (получить площадь поверхности).*/

interface Figure3d
{
    public function getVolume();

    public function getSurfaceSquare();
}

