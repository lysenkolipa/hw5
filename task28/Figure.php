<?php

namespace task28;

/** Задача 28.1: Сделайте интерфейс Figure (простая фигура), который будет иметь методы getSquare и getPerimeter. */
interface Figure
{
    public function getSquare();

    public function getPerimeter();
}

