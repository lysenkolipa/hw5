<?php

namespace task22;

/** Задача 22.1: Сделайте класс ArraySumHelper.
 * Задача 22.2: Добавьте в него метод, который будет принимать первым аргументом массив чисел, а вторым - число, во
 * сколько раз нужно умножить каждый из элементов массива. Метод должен возвращать итоговый массив с умноженными элементами.
 * Задача 22.3: Пусть дан массив с числами. Найдите с помощью класса ArraySumHelper сумму квадратов элементов этого массива.
 * Задача 22.4: Переделайте класс ArraySumHelper на работу с статикой.
 */
class ArraySumHelper
{
    private array $arrNum = [];
    private string $num;

    /**
     * @param array $arrNum
     */
    public function setArrNum(array $arrNum): void
    {
        $this->arrNum = $arrNum;
    }

    /**
     * @param int $num
     */
    public function setNum(int $num): void
    {
        $this->num = $num;
    }

    /**
     * @return array
     */
    public function getArrNum(): array
    {
        return $this->arrNum;
    }

    /**
     * @return string
     */
    public function getNum(): string
    {
        return $this->num;
    }

    public static function increaseNumber($num, $arrNum)
    {
        $newArr = [];
        foreach ($arrNum as $key => $value) {
            $newArr[] += $value * $num;
        }
        return $newArr;
    }
}

