<?php

namespace task18;

/**
 * Задача 18.3: Сделайте класс Employee (работник), в котором будут следующие свойства: name (имя) и surname (фамилия).
 * Пусть начальные значения этих свойств будут передаваться параметром в конструктор.
 * Задача 18.4: Сделайте геттеры и сеттеры для свойств name и surname.
 * Задача 18.5: Пусть теперь третьим параметром конструктора будет передаваться должность работника, представляющая собой
 * объект класса Post. Укажите тип этого параметра в явном виде.
 * Задача 18.6: Сделайте так, чтобы должность работника (то есть переданный объект с должностью) записывалась в свойство post.
 * Задача 18.9: Реализуйте в классе Employee метод changePost, который будет изменять должность работника на другую.
 * Метод должен принимать параметром объект класса Post. Укажите в методе тип принимаемого параметра в явном виде.
 */

class Employee extends Post
{
    private $name;
    private $surname;


    public function __construct(string $name, string $surname, Post $post, $salary)
    {
//        parent::__construct($post, $salary);
        $this->name = $name;
        $this->surname = $surname;
        $this->setPost($post);

    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @param string $surname
     */
    public function setSurname(string $surname): void
    {
        $this->surname = $surname;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * @param Post $post
     */
    public function setPost(Post $post): void
    {
        $this->post = $post;
    }

    /**
     * @param string $name
     * @param Post $post
     */
    public function changePost(string $name, Post $post)
    {
//        $post->parent::setName($name);
//        $post->parent::setPost($post);
    }
}