<?php

namespace task18;

/**Задача 18.1: Сделайте класс Post (должность), в котором будут следующие свойства, доступные только для чтения:
name (название должности) и salary (зарплата на этой должности).*/

class Post
{
    private $name;
    private $salary;

    public function __construct($name, $salary)
    {
        $this->name = $name;
        $this->salary = $salary;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getSalary()
    {
        return $this->salary;
    }
}