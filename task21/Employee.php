<?php

namespace task21;

/**
 * Задача 21.5: Сделайте класс Employee, который будет наследовать от класса User. Пусть новый класс имеет свойство salary,
 * в котором будет хранится зарплата работника. Зарплата должна передаваться четвертым параметром в конструктор объекта.
 * Сделайте также геттер для этого свойства.
 */
class Employee extends User
{
    private int $salary;

    /**
     * Employee constructor.
     * @param string $name
     * @param string $surname
     * @param $birthday
     * @param $salary
     */
    public function __construct(string $name, string $surname, $birthday, int $salary)
    {
        parent::__construct($name, $surname, $birthday);
        $this->salary = $salary;
    }

    /**
     * @return int
     */
    public function getSalary(): int
    {
        return $this->salary;
    }
}

