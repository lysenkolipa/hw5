<?php

namespace task21;

use DateTime;

/**
 * Задача 21.1: Сделайте класс User, в котором будут следующие свойства только для чтения: name (имя), surname (фамилия).
 * Начальные значения этих свойств должны устанавливаться в конструкторе. Сделайте также геттеры этих свойств.
 * Задача 21.2: Сделайте так, чтобы третьим параметром в конструктор передавалась дата рождения работника в формате год-месяц-день.
 * Запишите ее в свойство birthday. Сделайте геттер для этого свойства.
 * Задача 21.3: Сделайте приватный метод calculateAge, который параметром будет принимать дату рождения, а возвращать возраст
 * с учетом того, был ли уже день рождения в этом году, или нет.
 * Задача 21.4: Сделайте так, чтобы метод calculateAge вызывался в конструкторе объекта, рассчитывал возраст пользователя и
 * записывал его в приватное свойство age. Сделайте геттер для этого свойства.
 */
class User
{
    private string $name;
    private string $surname;
    private mixed $birthday;
    private int $age;

    public function __construct(string $name, string $surname, $birthday)
    {
        $this->name = $name;
        $this->surname = $surname;
        $this->age = $this->calculateAge($birthday);
    }

    /**
     * @return int
     */
    public function getAge(): int
    {
        return $this->age;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * @param false|string $birthday
     */
    public function setBirthday($birthday): void
    {
        $this->birthday = $birthday;
    }

    private function calculateAge($birthday)
    {
        $bday = new DateTime($birthday);
        $today = new Datetime(date('m.d.y'));
        $diff = $today->diff($bday);

        return $diff->y;
    }
}
