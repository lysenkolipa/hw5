<?php

namespace task29;

/**
 * Задача 29.1: У нас есть интерфейсы из задачи 28, плюс сделайте интерфейс Tetragon (четырехугольник), у которого будут
 * гетеры для для всех четырех сторон четырехугольника.
 */
interface Tetragon
{
    public function getSideA();

    public function getSideB();

    public function getSideC();

    public function getSideD();
}