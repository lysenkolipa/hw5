<?php

namespace task29;

use task28\Figure;

/** Задача 29.3: Сделайте так, чтобы класс Disk реализовывал два интерфейса: и Figure, и Circle.*/
class Disk implements Figure, Circle
{
    private int $radius;

    public function __construct($radius)
    {
        $this->radius = $radius;
    }

    public function getRadius()
    {
        return $this->radius;
    }

    public function getDiameter()
    {
        return $this->radius * $this->radius;
    }

    public function getSquare()
    {
        return 3.14 * $this->radius * $this->radius;
    }

    public function getPerimeter()
    {
        return 2 * 3.14 * $this->radius;
    }
}

