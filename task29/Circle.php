<?php

namespace task29;

/** Задача 29.1: Сделайте интерфейс Circle (круг) с методами getRadius (получить радиус) и getDiameter (получить диаметр).*/
interface Circle
{
    public function getRadius();

    public function getDiameter();
}