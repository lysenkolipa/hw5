<?php

namespace task29;
use task28\Figure;

/** Задача 29.2: Сделайте так, чтобы класс Rectangle реализовывал два интерфейса: и Figure, и Tetragon.*/

class Rectangle implements Figure, Tetragon
{
    private int $sideA;
    private int $sideB;

    public function __construct($sideA, $sideB)
    {
        $this->sideA = $sideA;
        $this->sideB = $sideB;
    }
    public function getSideA()
    {
        return $this->sideA;
    }
    public function getSideB()
    {
        return $this->sideB;
    }
    public function getSideC()
    {
        return $this->sideA;
    }
    public function getSideD()
    {
        return $this->sideB;
    }
    public function getSquare()
    {
        return $this->sideA * $this->sideB;
    }
    public function getPerimeter()
    {
        return 2 * $this->sideA + 2 * $this->sideB;
    }
}