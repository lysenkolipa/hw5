<?php

namespace task27;

/**
 * Задача 27.2: Сделайте интерфейс EmployeeInterface, наследующий от интерфейса UserInterface и добавляющий в него методы
 * getSalary и setSalary.
 */

interface EmployeeInterface extends UserInterface
{
    public function getSalary();

    public function setSalary($salary);
}