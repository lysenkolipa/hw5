<?php

namespace task27;

/** Задача 27.1: Сделайте интерфейс UserInterface с методами getName, setName, getAge, setAge. */
interface UserInterface
{
    public function getName();

    public function setName($name);

    public function getAge();

    public function setAge($age);
}