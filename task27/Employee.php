<?php

namespace task27;

/** Задача 27.3: Сделайте класс Employee, реализующий интерфейс EmployeeInterface.*/

class Employee implements EmployeeInterface
{
    private string $name;
    private int $age;
    private int $salary;

    public function __construct($name, $age, $salary)
    {
        $this->name = $name;
        $this->age = $age;
        $this->salary = $salary;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getAge(): int
    {
        return $this->age;
    }

    /**
     * @return int
     */
    public function getSalary(): int
    {
        return $this->salary;
    }


    public function setAge($age): void
    {
        $this->age = $age;
    }

    /**
     * @param string $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @param int $salary
     */
    public function setSalary($salary): void
    {
        $this->salary = $salary;
    }


}