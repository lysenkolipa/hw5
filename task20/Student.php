<?php

namespace task20;

/**
 * Задача 20.1: Есть класс Student из задачи 8. Если по ходу выполнения данного задания понадобятся новые свойства - создайте их.
 * Задача 20.3: Создайте новый класс (на ваше усмотрение), который будет наследоваться от класса Student .
 * Задача 20.4: Переопределите родительский метод валидатор, чтобы он проверял еще и отчество курс-координатора, помимо
 * родительской проверки.
 */

/** class Student from HW3 task8*/
class Student
{
    private string $name;
    private int $course;
    private $courseAdministrator;
    private string $surname;

    /**
     * Student constructor.
     * @param $name
     * @param $surname
     * @param $course
     */
    public function __construct($name, $course, $surname)
    {
        $this->name = $name;
        $this->surname = $surname;
        $this->course = $course;
    }

    /**
     * @param mixed $surname
     */
    public function setSurname($surname): void
    {
        $this->surname = $surname;
    }

    /**
     * @return mixed
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param mixed $course
     */
    public function setCourse($course)
    {
        $this->course = $course;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getCourse()
    {
        return $this->course;
    }

    /**
     * @return bool|int
     */
    public function transferToNextCourse()
    {
        return $this->isCourseCorrect() ? $this->course + 1 : false;
    }

    /**
     * @return bool
     */
    private function isCourseCorrect()
    {
        return $this->course >= 1 && $this->course <= 5 ? $this->course : false;
    }

    /**
     * @param $courseAdministrator
     */
    public function setCourseAdministrator($courseAdministrator)
    {
        $this->courseAdministrator = $courseAdministrator;
    }

    /**
     * @return mixed
     */
    public function getCourseAdministrator()
    {
        return $this->courseAdministrator;
    }

/** Задача 20.2: Добавьте в этот класс валидатор для проверки имени и фамилии курс-координатора, сделайте так, чтобы его
 * можно было использовать в классах наследниках.*/
    public function studentValidator(): bool
    {
        return (preg_match('/^[a-zA-Z0-9]+$/', $this->getName()) &&
                preg_match('/^[a-zA-Z0-9]+$/', $this->getSurname()) &&
                $this->getCourseAdministrator());
    }
}
