<?php

namespace task20;

/**
 * Задача 20.3: Создайте новый класс (на ваше усмотрение), который будет наследоваться от класса Student .
 * Задача 20.4: Переопределите родительский метод валидатор, чтобы он проверял еще и отчество курс-координатора, помимо
 * родительской проверки.
 */
class ExtramuralStudent extends Student
{
    private $patronymic;

    public function __construct($name, $course, $surname, $patronymic)
    {
        parent::__construct($name, $course, $surname);

        $this->patronymic = $patronymic;
    }

    /**
     * @return mixed
     */
    public function getPatronymic()
    {
        return $this->patronymic;
    }

    /**
     * @param mixed $patronymic
     */
    public function setPatronymic($patronymic): void
    {
        $this->patronymic = $patronymic;
    }

    public function studentValidator(): bool
    {
        if (parent::studentValidator()) {
            if (preg_match('/^[a-zA-Z0-9]+$/', $this->getPatronymic())) {
                return true;
            }
        }
        return true;
    }

    public function displayExtramuralStudent()
    {
        if ($this->studentValidator()) {
            echo $this->getName() . ' ' . $this->getSurname() . ' ' . $this->getPatronymic();
        }
    }

}