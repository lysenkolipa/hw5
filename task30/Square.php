<?php

namespace task30;

class Square extends Figures
{
    private int $sideA;

    /**
     * @param int $sideA
     */
    public function setSideA(int $sideA): void
    {
        $this->sideA = $sideA;
    }

    /**
     * @return int
     */
    public function getSideA(): int
    {
        return $this->sideA;
    }

    public function getSquare()
    {
        return $this->getSideA() * $this->getSideA();
    }

    public function getPerimeter()
    {
        return 4 * $this->getSideA();
    }

    public function getSum()
    {
        return $this->getSquare() + $this->getPerimeter();
    }
}