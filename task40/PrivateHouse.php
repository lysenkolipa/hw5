<?php


namespace task40;


class PrivateHouse
{
    private int $wall;
    private int $door;
    private int $windows;
    private int $roof;

    public function __construct($wall, $door, $windows, $roof)
    {
        $this->wall = $wall;
        $this->door = $door;
        $this->windows = $windows;
        $this->roof = $roof;
    }

    /**
     * @return int
     */
    public function getRoof(): int
    {
        return $this->roof;
    }

    /**
     * @return int
     */
    public function getDoor(): int
    {
        return $this->door;
    }

    /**
     * @return int
     */
    public function getWall(): int
    {
        return $this->wall;
    }

    /**
     * @return int
     */
    public function getWindows(): int
    {
        return $this->windows;
    }

    public function getLivingPlace(): void
    {
        echo 'This building contains: doors - ' . $this->getDoor() . ', walls - ' . $this->getWall() . ', roof - ' . $this->getRoof() . ', windows - ' . $this->getWindows();
    }
}

