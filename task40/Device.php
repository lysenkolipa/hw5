<?php

namespace task40;

abstract class Device
{
    private $door;
    private $wall;

    /**
     * @return mixed
     */
    public function getDoor()
    {
        return $this->door;
    }

    /**
     * @param mixed $door
     */
    public function setDoor($door): void
    {
        $this->door = $door;
    }

    /**
     * @return mixed
     */
    public function getWall()
    {
        return $this->wall;
    }

    /**
     * @param mixed $wall
     */
    public function setWall($wall): void
    {
        $this->wall = $wall;
    }

    abstract function getDeviceFunction();
}

