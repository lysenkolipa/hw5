<?php


namespace task40;

class Fridge extends Device
{
    private string $function;

    /**
     * @param mixed $function
     */
    public function setFunction($function): void
    {
        $this->function = $function;
    }

    public function getDeviceFunction()
    {
        return $this->function;
    }
}

