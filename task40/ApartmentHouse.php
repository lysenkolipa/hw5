<?php

namespace task40;


class ApartmentHouse extends Building
{
    private int $wall;
    private int $door;
    private int $windows;
    private int $roof;
    private int $floor;

    public function __construct($wall, $door, $windows, $roof, $floor)
    {
        $this->wall = $wall;
        $this->door = $door;
        $this->windows = $windows;
        $this->roof = $roof;
        $this->floor = $floor;
    }

    /**
     * @return int
     */
    public function getRoof(): int
    {
        return $this->roof;
    }

    /**
     * @return int
     */
    public function getWindows(): int
    {
        return $this->windows;
    }

    public function getLivingPlace(): void
    {
        echo 'This building contains: doors - ' . $this->getDoor() . ', walls - ' . $this->getWall() . ', roof - ' . $this->getRoof() . ', windows - ' . $this->getWindows() . ', floors - ' . $this->getFloor();
    }
}

