<?php

namespace task40;

class Dugout extends Building
{
    private int $door;
    private int $wall;
    private int $floor;

    public function __construct($door, $wall, $floor)
    {
        $this->door = $door;
        $this->wall = $wall;
        $this->floor = $floor;
    }

    public function getLivingPlace(): void
    {
        echo 'This building contains: doors - ' . $this->getDoor() . ', walls - ' . $this->getWall() . ', floors - ' . $this->getFloor();
    }
}

