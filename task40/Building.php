<?php

namespace task40;

/** Задача 40.1: Есть класс Building (здание). Нам нужно создать класс PrivateHouse (частный дом), класс ApartmentHouse
 * (многоквартирный дом), класс Dugout (землянка), класс Fridge (холодильник), класс Wardrobe (шкаф), класс Microwave
 * (микроволновка).
 * Задача 40.2: У всех этих классов есть некоторые схожие элементы и функционал (к примеру, двери/окна, которые могут
 * открываться/закрываться). Ваша задача создать все эти классы таким образом, чтобы уменьшить до минимума повторение
 * кода и оптимизировать его использование. Помните о различиях в является и имеет.
 */
abstract class Building
{
    private string $door;
    private string $wall;
    private string $floor;

    abstract function getLivingPlace(): void;

    /**
     * @return mixed
     */
    public function getDoor()
    {
        return $this->door;
    }

    /**
     * @return mixed
     */
    public function getWall()
    {
        return $this->wall;
    }

    /**
     * @return mixed
     */
    public function getFloor()
    {
        return $this->floor;
    }
}

