<?php


namespace task40;


class Wardrobe extends Device
{
    private string $function;

    /**
     * @param string $function
     */
    public function setFunction(string $function): void
    {
        $this->function = $function;
    }

    /**
     * @return string
     */
    public function getDeviceFunction()
    {
        return $this->function;
    }
}

