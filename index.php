<?php

declare(strict_types=1);

use classes\ShowFiche;
use classPension\pensionFond;
use devswb\hometask2\discriminantCalc\DiscriminantCalc;
use lysenkolipa\calcliba\fuelCalculator\FuelCalculator;
use Nechiporenko\Classes\Main\SimpleCalc;
use pelukho\userfaker\src\UserFaker;
use Platon\Method\Ordinary;
use psydo\packagisttest\dumbclass\Test1;
use task17\Employee;
use task17\City;
use task17\User;
use task18\Employee as Employee1;
use task18\Post;
use task20\ExtramuralStudent;
use task22\ArraySumHelper;
use task25\Cube;
use task25\Quadrate;
use task31\FiguresCollection;
use task32\Country;
use task32\User as User1;
use task33\Test;
use winterpsv\winter\Calc;

require_once __DIR__ . '/vendor/autoload.php';

/**
 * Задача 17.4: Создайте 3 объекта класса User, 3 объекта класса Employee, 3 объекта класса City, и в произвольном
 * порядке запишите их в массив $arr.
 */

echo "<i>Task17</i><br><br>";

/** @var User $user1 */
$user1 = new User();
$user1->setName("Ivan");
$user1->setSurname("Ivanov");

/** @var User $user2 */
$user2 = new User();
$user2->setName("Tamara");
$user2->setSurname("Toriy");

/** @var User $user3 */
$user3 = new User();
$user3->setName("Anna");
$user3->setSurname("Petrenko");


/** @var Employee $employee1 */
$employee1 = new Employee();
$employee1->setName("Roman");
$employee1->setSurname("Petrov");
$employee1->setSalary(1600);


/** @var Employee $employee2 */
$employee2 = new Employee();
$employee2->setName("Sergiy");
$employee2->setSurname("Rumyan");
$employee2->setSalary(2100);

/** @var Employee $employee3 */
$employee3 = new Employee();
$employee3->setName("Inga");
$employee3->setSurname("Teran");
$employee3->setSalary(2300);

/** @var City $city1 */
$city1 = new City();
$city1->setName("Kharkiv");
$city1->setPopulation(1240);

/** @var City $city2 */
$city2 = new City();
$city2->setName("Uman");
$city2->setPopulation(850);

/** @var City $city3 */
$city3 = new City();
$city3->setName("Kherson");
$city3->setPopulation(1180);

/** @var array $arr */
$arr = array($user1, $employee1, $city1, $user2, $employee2, $city2, $user3, $employee3, $city3);

/** Задача 17.5: Переберите циклом массив $arr и выведите на экран столбец свойств name тех объектов, которые принадлежат
 * классу User или потомку этого класса.*/
foreach ($arr as $key => $value) {
    if ($value instanceof User) {
        echo $value->getName() . "<br>";
    }
}
echo "- - - - - - - -<br>";

/** Задача 17.6: Переберите циклом массив $arr и выведите на экран столбец свойств name тех объектов, которые НЕ принадлежат
 * классу User или потомку этого класса.*/
foreach ($arr as $key => $value) {
    if (!($value instanceof User)) {
        echo $value->getName() . "<br>";
    }
}
echo "- - - - - - - -<br>";

/**
 * Задача 17.7: Переберите циклом массив $arr и выведите на экран столбец свойств name тех объектов, которые принадлежат
 * именно классу User, то есть не классу City и не классу Employee.*/


/** Задача 18.2: Создайте несколько объектов класса Post: программист, менеджер водитель. */

echo "<br><i>Task18</i><br><br>";

/** @var Post $post1 */
$post1 = new Post('programmer', 3000);

/** @var Post $post2 */
$post2 = new Post('manager', 1500);

/** @var Post $post3 */
$post3 = new Post('driver', 800);

/**  Задача 18.7: Создайте объект класса Employee с должностью программист. При его создании используйте один из
 * объектов класса Post, созданный нами ранее. */

/** @var \task18\Employee $employee4 */
$employee4 = new Employee1('Andrey', 'Gavrylchenko', $post1, 1000);

/** Задача 18.8: Выведите на экран имя, фамилию, должность и зарплату созданного работника.*/
echo $employee4->getName() . " ";
echo $employee4->getSurname() . "<br>";
echo $employee4->getName();
echo $employee4->getSalary();
echo "<br>- - - - - - - -<br>";

echo "<i>Task20</i><br><br>";
$extramuralStudent = new ExtramuralStudent('Petro', 5, 'Ivanov', 'Ivanovych');

echo $extramuralStudent->displayExtramuralStudent() . '<br>';
echo "<br>- - - - - - - -<br>";

echo "<i>Task21</i><br><br>";
$user21 = new \task21\User('Lisa', 'Larko', '21.09.1990');
echo "User surname: " . $user21->getSurname() . '<br>';
echo "User name: " . $user21->getName() . '<br>';
echo "User age: " . $user21->getAge() . ' years';

$employee21 = new \task21\Employee('Tamara', 'Grobenko', '01.01.2000', 1800);
echo "<br>Employee surname: " . $employee21->getSurname() . " ";
echo "<br>Employee name: " . $employee21->getName() . " ";
echo "<br>Employee age: " . $employee21->getAge() . " ";
echo "<br>Employee salary: " . $employee21->getSalary();
echo "<br>- - - - - - - -<br>";

echo "<i>Task22</i><br><br>";
$m = new ArraySumHelper();
$arr = [1, 2, 3, 4, 5];
print_r(ArraySumHelper::increaseNumber(2, $arr));

echo "<br>- - - - - - - -<br>";

echo "<i>Task23</i><br><br>";
$num = new \task23\Num();
echo 'Sum of numbers: ' . $num->getSum() . '<br>';
$num->displayConst();
echo "<br><br>- - - - - - - -<br>";

echo "<i>Task24</i><br><br>";
$user24 = new \task24\User();
$user24->setAge(19);
$user24->setName('Ivan');
echo "Name: " . $user24->getName() . " age: " . $user24->getAge();
echo "<br><br>- - - - - - - -<br>";

echo "<i>Task25</i><br><br>";
$cube = new Cube(8);
echo "Cube Square = " . $cube->getCubeSquare() . "<br>";
echo "Cube Volume = " . $cube->getCubeVolume();
echo "<br><br>- - - - - - - -<br>";

/** * Задача 28.4: Создайте несколько объектов класса Quadrate, несколько объектов класса Rectangle и несколько объектов
 * класса Cube (внимательно продумайте используемые интерфейсы). Запишите их в массив $arr в случайном порядке.
 * Задача 28.5: Переберите циклом массив $arr и выведите на экран только площади объектов реализующих интерфейс Figure.
 * Задача 28.6: Переберите циклом массив $arr и выведите для плоских фигур их площади, а для объемных - площади их поверхности.
 */
echo "<i>Task28</i><br><br>";

$quadrate1 = new Quadrate(4);
$cube2 = new \task28\Cube(4);
$quadrate3 = new Quadrate(4);
$cube4 = new \task28\Cube(4);

$arr1 = [$quadrate1, $cube2, $quadrate3, $cube4];
print_r($arr1);

foreach ($arr1 as $value) {
    if ($value instanceof \task28\Figure) {
        echo '<br>' . $value->getSquare();
    }
}
echo "<br><br>- - - - - - - -<br>";

/** Задача 32.1: Реализуйте трейт Helper, содержащий приватные свойства name и age, а также их геттеры.
 * Задача 32.2: Реализуйте класс Country (страна) со свойствами name (название), age (возраст), population (количество населения) и геттерами (по надобности) для них.
 * Задача 32.3: Реализуйте класс User, в конструкторе которого задаются свойства name и age по аналогии с задачей 32.2.
 * Задача 32.4: Пусть наш классы для сокращения своего кода используют, уже созданный в задаче 32.1, трейт Helper.
 * При надобности, отрефакторите код наших классов. */

echo "<i>Task32</i><br><br>";



/** @var Country $country */
$country = new Country('USA', 244, 335371782);
echo 'Country name: ' . $country->getName() . '<br>';
echo 'Country age: ' . $country->getAge() . '<br>';
echo 'Country population: ' . $country->getPopulation() . '<br>';
echo "<br>- - - - - - - -<br>";

/** @var \task32\User $user32 */
$user32 = new User1('Roman', 32);
echo $user32->getName() . ' ';
echo $user32->getAge() . '<br>';
echo "<br>- - - - - - - -<br>";

/** Задача 33.1: Сделайте 3 трейта с названиями: Trait1, Trait2 и Trait3. Пусть в первом трейте будет метод method1, возвращающий 1,
 * во втором трейте - метод method2, возвращающий 2, а в третьем трейте - метод method3, возвращающий 3.
 * Пусть все эти методы будут приватными.
 * Задача 33.2: Сделайте класс Test, использующий все три созданных нами трейта.
 * Сделайте в этом классе публичный метод getSum, возвращающий сумму результатов методов подключенных трейтов.*/

echo "<i>Task33</i><br><br>";

/** @var Test $test */
$test = new Test();
echo 'Test Sum: ' . $test->getSum() . '<br>';
echo "<br>- - - - - - - -<br>";


echo "<i>Task45</i><br><br>";

$calc = new FuelCalculator(389, 9.0, 32.1);
echo $calc->getFuelCostPerTrip();

$a = new pensionFond();
$a->name = 'Ivan';
$a->age = 57;
$a->city = 'Kyiv';
$a->showCity();

$c = new DiscriminantCalc(4, 67, 3);
echo $c->discriminant() . "<br>";

$b = new Calc();
$b->counts('circle', 4.9, 4.0);

$d = new SimpleCalc();
$d->add(12345, 9576);

$f = new ShowFiche('lorem');
$f->showFiche();

$v = new Test1();
$v->fucntionprint('Lorem Ipsum');

$n = new UserFaker('fmale', 'ukraine');
$n->getUserThumbnail();

$h = new Ordinary(567);
echo $h->getInputParams();

