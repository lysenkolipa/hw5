<?php

namespace task24;

/** Задача 24.1:: Пусть у нас дан такой интерфейс UserInterface - https://gist.github.com/aksafan/973157d6ddb36dd24e3e901b7e9a5fcf */

interface UserInterface
{
    public function setName($name); // установить имя

    public function getName(); // получить имя

    public function setAge($age); // установить возраст

    public function getAge(); // получить возраст
}