<?php

namespace task24;

/** Задача 24.2: Сделайте класс User, который будет реализовывать данный интерфейс. */

class User implements UserInterface
{
    private $age;
    private $name;

    public function setAge($age)
    {
        $this->age = $age;
    }

    public function setName($name)
    {
       $this->name = $name;
    }

    public function getAge()
    {
        return $this->age;
    }

    public function getName()
    {
        return $this->name;
    }
}