<?php

namespace task17;

/**
 * Задача 17.2: Сделайте класс Employee, который будет наследовать от класса User и добавлять salary (зарплата).
 */
class Employee extends User
{
    private int $salary;

    /**
     * @param int $salary
     */
    public function setSalary($salary): void
    {
        $this->salary = $salary;
    }

    /**
     * @return int
     */
    public function getSalary(): int
    {
        return $this->salary;
    }
}

