<?php

namespace task17;

/**
 * Задача 17.3: Сделайте класс City с публичными свойствами name (название города) и population (количество населения).
 */
class City
{
    public string $name;
    public int $population;

    /**
     * @param mixed $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getPopulation(): int
    {
        return $this->population;
    }

    /**
     * @param mixed $population
     */
    public function setPopulation(int $population): void
    {
        $this->population = $population;
    }
}

