<?php

namespace task17;

/**
 * Задача 17.1: Сделайте класс User с публичным свойствами name (имя) и surname (фамилия).
 */
class User
{
    public string $name;
    public string $surname;

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @param string $surname
     */
    public function setSurname(string $surname): void
    {
        $this->surname = $surname;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getSurname(): string
    {
        return $this->surname;
    }
}

