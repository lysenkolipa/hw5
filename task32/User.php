<?php

namespace task32;

class User
{
    use Helper;

    public function __construct($name, $age)
    {
        $this->name = $name;
        $this->age = $age;
    }
}

