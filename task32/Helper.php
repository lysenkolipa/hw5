<?php

namespace task32;

//Задача 32.1: Реализуйте трейт Helper, содержащий приватные свойства name и age, а также их геттеры.

trait Helper
{
    private $name;
    private $age;

    public function getName()
    {
        return $this->name;
    }

    public function getAge()
    {
        return $this->age;
    }
}
