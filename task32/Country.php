<?php

namespace task32;

/** Задача 32.2: Реализуйте класс Country (страна) со свойствами
 * name (название), age (возраст), population (количество населения) и геттерами (по надобности) для них. */
class Country
{
    private string $name;
    private int $age;

    use Helper;

    private int $population;

    public function __construct($name, $age, $population)
    {
        $this->name = $name;
        $this->age = $age;
        $this->population = $population;
    }

    public function getPopulation()
    {
        return $this->population;
    }
}

