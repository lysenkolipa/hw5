<?php

namespace task31;

use task30\Rectangle;
use task30\Square;

/** * Задача 31.2: Сделайте класс FiguresCollection, который будет хранить в себе массив объектов-фигур.
 * Задача 31.3: Сделайте в классе FiguresCollection метод addFigure для добавления объектов в коллекцию (в массив
 * свойства класса). Позаботьтесь о том, чтобы данный метод 100% принимал аргументом именно какую-то из фигур (любую).
 * Задача 31.4: Сделайте в классе FiguresCollection метод getTotalSquare, находящий полную площадь фигур всей коллекции
 * (всего массива). Подсказка: у каждой фигуры должен быть способ найти площадь.*/
class FiguresCollection
{
    private array $figArr = [];

    public function addFigure(object $figure)
    {
        $this->figArr[] = $figure;
    }

    public function getTotalSquare()
    {
        $count = 0;
        foreach ($this->figArr as $value) {
            if ($value instanceof Rectangle) {
                $value->getSquare();
                $count += $value->getSquare();
            }
            if ($value instanceof Square) {
                $value->getSquare();
                $count += $value->getSquare();
            }
        }
        return $count;
    }
}