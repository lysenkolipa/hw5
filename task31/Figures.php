<?php

namespace task31;

/** Задача 31.1: Реализуйте архитектуру из задачи 30, теперь используя интерфейсы. */
interface Figures
{
    public function getSquare();

    public function getPerimeter();

    public function getSum();
}