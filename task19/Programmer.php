<?php

namespace task19;

/**
Задача 19.3: Сделайте класс Programmer, который будет наследовать от класса Employee. Пусть новый класс имеет свойство
langs, в котором будет хранится массив языков, которыми владеет программист. Сделайте также геттер и сеттер для этого свойства.
 */

class Programmer extends Employee
{
    private $langs = [];


    public function setLangs(array $langsArr)
    {
        $this->langs = array_merge($this->langs, $langsArr);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLangs()
    {
        return $this->langs;
    }
}