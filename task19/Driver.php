<?php

namespace task19;

/**
 * Задача 19.4: Сделайте класс Driver (водитель), который будет наследовать от класса Employee. Пусть новый класс добавляет
 * следующие свойства: водительский стаж, категория вождения (A, B, C, D), а также геттеры и сеттеры к ним.
 */

class Driver extends Employee
{
    private $drivingExperience;
    private $category = [];

    /**
     * @param $category
     * @return $this
     */
    public function setCategory($category)
    {
        $this->category [] = $category;
        return $this;
    }

    /**
     * @param $drivingExperience
     * @return $this
     */
    public function setDrivingExperience($drivingExperience)
    {
        $this->drivingExperience = $drivingExperience;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @return mixed
     */
    public function getDrivingExperience()
    {
        return $this->drivingExperience;
    }
}

