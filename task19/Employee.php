<?php

namespace task19;

use task17\User;

/**
 * Задача 19.1: Есть класс User из задачи 3.
 * Задача 19.2: Сделайте класс Employee, который будет наследовать от класса User и расширьте его 1 новым свойством и 1 новым
 * методом.
 * Задача 19.5: Добавьте в класс User 1 свойство и 2 метода для него, которые нельзя будет использовать снаружи класса, но
 * можно будет использовать в классе  Driver. Используйте это свойство и методы.
 * Задача 19.6: Сделайте аналогичные действия (как в задаче 19.5), только для класса Employee и Programmer соответственно.
 */
class Employee extends User
{
    private $salary;

    /**
     * @param mixed $salary
     */
    public function setSalary($salary): void
    {
        $this->salary = $salary;
    }

    /**
     * @return mixed
     */
    public function getSalary()
    {
        return $this->salary;
    }

    public function displayEmploy()
    {
        echo $this->getName() . " " . $this->getSurname() . " " . $this->getSalary() . "<br>";
    }
}