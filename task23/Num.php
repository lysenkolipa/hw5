<?php

namespace task23;

/**
 * Задача 23.1: Сделайте класс Num, у которого будут два публичных статических свойства: num1 и num2.
 * Запишите в первое свойство число 2, а во второе - число 3. Выведите сумму значений свойств на экран.
 * Задача 23.2: Добавьте в класс Num, два приватны статических свойства: num3 и num4. Пусть по умолчанию в свойстве num3
 * хранится число 3, а в свойстве num4 - число 5.
 * Задача 23.3: Добавьте в класс Num метод getSum, который будет выводить на экран сумму значений свойств num3 и num1.
 * Задача 23.4: Добавьте в класс Num две константы (названия и значения придумайте сами исходя из логики использования констант).
 * Выведите на экран значение первой константы напрямую, и значение второй константы через геттер.
 */
class Num
{
    private const NUM = 9;
    private const ID = 1;

    public static $num1 = 2;
    public static $num2 = 3;

    private static $num3 = 3;
    private static $num4 = 5;

    /**
     * @return int
     */
    public function getSum()
    {
        return self::$num3 + self::$num1;
    }

    /**
     * @return int
     */
    public function getID()
    {
        return self::ID;
    }

    public function displayConst()
    {
        echo self::NUM;
        echo $this->getID();
    }
}

