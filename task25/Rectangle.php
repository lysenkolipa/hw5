<?php


namespace task25;


use task28\Figure;

class Rectangle implements Figure
{
    private const NUM = 2;

    private int $sideA;
    private int $sideB;

    /**
     * Rectangle constructor.
     * @param $sideA
     * @param $sideB
     */
    public function __construct($sideA, $sideB)
    {
        $this->sideA = $sideA;
        $this->sideB = $sideB;
    }

    /**
     * @return int
     */
    public function getSideA(): int
    {
        return $this->sideA;
    }

    /**
     * @return int
     */
    public function getSideB(): int
    {
        return $this->sideB;
    }

    public function getSquare()
    {
        return $this->getSideA() * $this->getSideB();
    }

    public function getPerimeter()
    {
        return self::NUM * ($this->getSideA()+ $this->getSideB());
    }
}