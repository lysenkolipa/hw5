<?php

namespace task25;

/**
 * Задача 25.3: Сделайте класс Cube, реализующий интерфейс CubeInterface.
 */

class Cube implements CubeInterface
{
    private const NUMBER = 6;

    private $side;

    /**
     * Cube constructor.
     * @param $side
     */
    public function __construct($side)
    {
        $this->side = $side;
    }

    /**
     * @param float|int $side
     */
    public function setSide($side): void
    {
        $this->side = $side;
    }

    /**
     * @return float|int
     */
    public function getSide()
    {
        return $this->side;
    }

    /**
     * @return float|int
     */
    public function getCubeSquare()
    {
        return self::NUMBER * ($this->getSide() * $this->getSide());
    }

    /**
     * @return float|int
     */
    public function getCubeVolume()
    {
        return $this->getSide() * $this->getSide() * $this->getSide();
    }
}
