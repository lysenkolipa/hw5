<?php

namespace task25;

use task28\Figure;

class Quadrate implements Figure
{
    private const NUM = 4;

    private int $side;

    public function __construct($side)
    {
        $this->side = $side;
    }

    /**
     * @return int
     */
    public function getSide(): int
    {
        return $this->side;
    }

    /**
     * @param int $side
     */
    public function setSide(int $side): void
    {
        $this->side = $side;
    }

    public function getSquare()
    {
        return $this->getSide() * $this->getSide();
    }

    public function getPerimeter()
    {
        return self::NUM * $this->getSide();
    }
}

