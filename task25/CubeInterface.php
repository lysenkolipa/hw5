<?php
namespace task25;

/**
 * Задача 25.1: Сделайте интерфейс CubeInterface, который будет описывать фигуру Куб.
 * Задача 25.2: Пусть ваш интерфейс описывает конструктор, параметром принимающий сторону куба, а также методы для получения
 * объема куба и площади поверхности.
 */

interface CubeInterface
{
    public function __construct($side);

    public function getCubeVolume();

    public function getCubeSquare();
}